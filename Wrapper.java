package hw04;

import java.util.*;


class MyCustomException extends Exception
{
    public MyCustomException(String message)
    {
        super(message);
    }
}

class Wrapper<T> {
    private List<T> list =  new LinkedList<>();

    public T getItem (int index) {
        return list.get(index);
    }

    public void addItem(T value) throws MyCustomException{

        for(T t : list) {
            if (value == null) {
                throw new MyCustomException("Value of Item Can Not Be Null");
            }
            if (t == value) {
                throw new MyCustomException("Value Of Item Can Not Duplicate" + "(Value : " + value + " is duplicated)");
            }
        }
        list.add(value);
    }

    public int size() {
        return list.size();
    }
}
