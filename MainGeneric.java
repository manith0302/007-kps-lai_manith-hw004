package hw04;

import hw04.Wrapper;

class MainGeneric {
    public static void main (String[] args) {
        Wrapper<Integer> itemTypeInt = new Wrapper<>();

        try {

            itemTypeInt.addItem(1);
            itemTypeInt.addItem(2);

//            for (int add=0 ; add<10000; add++){
//                itemTypeInt.addItem(add);
//            }

        }catch (MyCustomException e){
            e.printStackTrace();
        }

        System.out.println("Value of items inputting : ");
        for (int i=0; i<itemTypeInt.size() ;i++){
            System.out.print(itemTypeInt. getItem(i));
            System.out.println();
        }
    }
}
